# PoGoAccountCheck
PoGoAccountCheck is a python script which reads PTC accounts from a file and checks if they are banned. If they __are__ banned, the program will save them to a file, banned.txt based on the fork by sebastienvercammen

# Installation
To Install PoGoAccountCheck run:

    git clone https://georgeherby@bitbucket.org/georgeherby/pogoaccountchecker.git

Then:

	cd PoGoAccountCheck

After you are in the directory, make sure to install all the requirements by running:

	pip install -r requirements.txt

# Usage

To use the program run the following command:
```
1. Git clone/pull (i have just updated something)
2. Copy all account files you want checking in to /to_check/
3. In the spare_accounts.csv in the same folder at banned.py copy all the accounts you want to be available to replace 
banned accounts
4. Then run the batch file

NOTE - after it has completed - it will output the new account file to /not_banned/ and accounts that were banned or 
failed go to their respective folders.

NOTE - if it fails halfway through or you rerun it, be sure to revert spare_accounts to the original contents 
(as this will have been updated and reduced in size once accounts are put into the new accounts files in /not_banned/) 
and clear out any files in the /not_banned/, /banned/ or /failed/ otherwise it will append to the files in there. 
(forgot the ability to delete existing files in the those folder will do as some point. The /to_check/ files is untouched 
so will be the same as as the begining
```

# Formatting
Your file of accounts should be structured as follows:
```
	ptc,username1,password1
	ptc,username2,password2
	google,username3,password3
```

# Input
There are 2 input files needed to run

`spare_accounts.csv`    - This is in the same folder as the banned.py (unless set in the commands parameters)

`/tocheck/*.csv`        - This is in the folder to read in the accounts to be check on area by area basis. This will 
alwasy exlude files ending HIGHLEVEL.csv


# Output
There will be multiple outputs from the job in many folders

`/not_banned/` - this is the final list of accounts to use for an area

`/banned/`     - this is the list per area of the accounts that are banned including the ones its tried to add to the not_banned area file

`/failed/`     - this is the list per area of the accounts that are failing including the ones its tried to add to the not_banned area file