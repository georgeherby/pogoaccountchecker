# !/usr/bin/python
#  -*- coding: utf-8 -*-

"""banned.py - Check multiple PTC accounts ban status with Pokemon Go."""

from pgoapi import PGoApi
from pgoapi.exceptions import ServerSideRequestThrottlingException
from pgoapi.exceptions import BannedAccountException
from pgoapi.exceptions import NotLoggedInException
from pgoapi.exceptions import AuthTimeoutException
from pgoapi.exceptions import AuthException
from pgoapi.exceptions import NianticThrottlingException
import time
import sys
import os
import argparse
import re

def parse_arguments(args):
    """Parse the command line arguments for the console commands.
    Args:
      args (List[str]): List of string arguments to be parsed.
    Returns:
      Namespace: Namespace with the parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description='Pokemon Trainer Club Banned Account Checker'
    )
    parser.add_argument(
        '-c', '--check', type=str, default='./to_check/', required=False,
        help='Folder containing file(s) with accounts to check in.'
    )
    parser.add_argument(
        '-o', '--output', type=str, default='./not_banned/', required=False,
        help='Folder to output file(s) with accounts that are not banned.'
    )
    parser.add_argument(
        '-f', '--failed', type=str, default='./failed/', required=False,
        help='Folder to output file(s) with accounts that failed to log in.'
    )
    parser.add_argument(
        '-b', '--banned', type=str, default='./banned/', required=False,
        help='Folder to output file(s) with accounts that are banned.'
    )
    parser.add_argument(
        '-l', '--location', type=str, default="40.7127837 -74.005941",
        required=False,
        help='Location to use when checking if the accounts are banned.'
    )
    parser.add_argument(
        '-hk', '--hash-key', type=str, default=None, required=True,
        help='Key for hash server.'
    )
    parser.add_argument(
        '-s', '--spare', type=str, default="spare_accounts.csv",
        required=False,
        help='File including path that contains any spare accounts that can be used when refilling account lists'
    )
    parser.add_argument(
        '-p', '--proxy', type=str,
        required=False,
        help='Add one proxy to use for requests'
    )
    return parser.parse_args(args)


def check_account(provider, username, password, location, api, filename, args):
    level = 0
    api.set_position(location[0], location[1], 0.0)
    i = 1
    while i < 3:
        try:
                if args.proxy:
                    proxies = {
                        'http': args.proxy,
                        'https': args.proxy,
                    }
                    api.set_authentication(provider=provider, username=username, password=password, proxy_config=proxies)
                else:
                    api.set_authentication(provider=provider, username=username, password=password)

                time.sleep(1)
                req = api.create_request()
                req.get_inventory()
                response = req.call()

                if type(response) is NotLoggedInException:
                    i += 1
                    print ('Failed - looping again ' + username)
                    time.sleep(5)

                elif response['status_code'] == 3:
                    __accountBanned(provider, username, password, filename, args)
                    return False

                elif response['status_code'] != 3:
                    player = api.get_inventory()
                    inventory_items = player['responses']['GET_INVENTORY']['inventory_delta']['inventory_items']
                    for item in inventory_items:
                        if 'player_stats' in item['inventory_item_data']:
                            player_stats = item['inventory_item_data']['player_stats']
                            level = player_stats['level']

                    __accountWorking(provider, username, password, filename, args, level)
                    return True

        except BannedAccountException:
            __accountBanned(provider, username, password, filename, args)
            return False
        except AuthTimeoutException:
            i += 1
            print ('Failed with timeout - looping again ' + username)
            time.sleep(10)
        except AuthException:
            i += 1
            print ('Failed with auth - looping again ' + username)
            time.sleep(10)
        except (NianticThrottlingException, ServerSideRequestThrottlingException):
            i += 1
            print ('Failed due to throttling. Pausing for 10 second and looping again ' + username)
            time.sleep(10)

    if i == 3:
        print ('Failed ' + str(i) + ' times. Failed for ' + username)
        __accountFailed(provider, username, password, filename, args)
        return False


def __accountBanned(provider, username, password, filename, args):
    path = args.banned
    print('{} is banned'.format(username))
    global number_to_replace
    appendFile(path, provider + ',' + username + ',' + password, filename+'_banned.csv')


def __accountFailed(provider, username, password, filename, args):
    path = args.failed
    print('{} failed to log in'.format(username))
    global number_to_replace
    appendFile(path, provider + ',' + username + ',' + password, filename+'_failed.csv')


def __accountWorking(provider, username, password, filename, args, level):
    path = args.output
    print('{} is not banned (level: {})...'.format(username, level))
    appendFile(path, provider + ',' + username + ',' + password, filename+'.csv')


def appendFile(path, text, filename):
    if os.path.exists(path+filename):
        f = open(path + filename, 'a+b')
    else:
        f = open(path + filename, 'w+b')

    f.write("%s\n" % text)

    f.close()


def entry():
    args = parse_arguments(sys.argv[1:])
    api = PGoApi()

    if args.proxy:
        print ('Using Proxy ' + args.proxy)
        proxies = {
            'http': args.proxy,
            'https': args.proxy,
        }
        api.set_proxy(proxies)

    prog = re.compile("^(\-?\d+\.\d+),?\s?(\-?\d+\.\d+)$")
    res = prog.match(args.location)
    if res:
        print('Using the following coordinates: {}'.format(args.location))
        position = (float(res.group(1)), float(res.group(2)), 0)
    else:
        print(('Failed to parse the supplied coordinates ({}).'
               + ' Please try again.').format(args.location))
        return

    if args.hash_key:
        print "Using hash key: {}.".format(args.hash_key)
        api.activate_hash_server(args.hash_key)

    path = args.check
    for fn in os.listdir(path):
        if fn.endswith(".csv") and not fn.endswith("HIGHLVL.csv"):
            number_to_replace = 0
            print(fn)
            filename = fn.split(".")[0]
            with open(path+fn) as f:
                credentials = [x.strip().split(',')[0:] for x in f.readlines()]

            for provider, username, password in credentials:
                    if not check_account(provider, username, password, position, api, filename, args):
                        number_to_replace += 1

            if number_to_replace > 0:
                source = open(args.spare, 'r')
                i = 1
                with source as f:
                    credentials = [x.strip().split(',')[0:] for x in f.readlines()]

                for provider, username, password in credentials:
                    if i <= number_to_replace:
                        if check_account(provider, username, password, position, api, filename, args):
                            print ('Account being added ' + username + ' is ok!')
                            i += 1
                        else:
                            print ('Account being added ' + username + ' is banned/failed!')
                            number_to_replace -= 1
                    else:
                        appendFile('./', provider + ',' + username + ',' + password, 'newSpares.csv')

                if os.path.exists(filename + '_sees_this_spare_accounts.csv'):
                    os.remove(filename + '_sees_this_spare_accounts.csv')

                os.rename('spare_accounts.csv', filename + '_sees_this_spare_accounts.csv')
                os.rename('newSpares.csv', 'spare_accounts.csv')


entry()
